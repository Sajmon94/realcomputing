﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RealComputing
{
    class Chromosome
    {
        public List<double> Genes = new List<double>();
        private int Size { get; set; }
        public double fitness;
        static private Random randomizer = new Random();
        public Chromosome(int count)
        {
            Genes.Capacity = count;
            Size = count;
        }
        public void UpdateIndividualFitness(Func<List<double>, double> function)
        {
            fitness = function(Genes);
        }
        public void RandomizeGenes(double min, double max)
        {
            for (int i = 0; i < Size; i++)
            {
                Genes.Add(randomizer.NextDouble() * (max - min) + min);
            }
        }
        public void SingleMutation()
        {
            for (int i = 0; i < Genes.Count; i++)
            {
                Genes[i] += (double)(randomizer.Next(-100, 100)) / 10000;
            }
        }
        public void ShowIndividual()
        {
            Console.WriteLine("Genes:");
            foreach (var item in Genes)
            {
                Console.WriteLine($"    {item}");
            }
            Console.WriteLine($"Fitness={fitness}");
            Console.WriteLine();
        }
        public Chromosome Clone()
        {
            Chromosome ch = new Chromosome(Size);
            ch.Genes = new List<double>();
            for (int i = 0; i < this.Genes.Count; i++)
            {
                ch.Genes.Add(this.Genes[i]);
            }
            return ch;
        }
    }
}
