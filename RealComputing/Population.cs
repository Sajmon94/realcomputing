﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RealComputing
{
    class Population
    {
        List<Chromosome> population = new List<Chromosome>();
        public Population(List<Chromosome> list)
        {
            population = list;
        }
        public Population(int count,int genesCount,int rangeMin,int rangeMax)
        {
            for (int i = 0; i < count; i++)
            {
                Chromosome ch = new Chromosome(genesCount);
                ch.RandomizeGenes(rangeMin, rangeMax);
                population.Add(ch);
            }
        }
        public void AddPopulations(Population secondary)
        {
            population.AddRange(secondary.population);
        }
        public void RemoveHalf()
        {
            population.RemoveRange(population.Count / 2, population.Count / 2);
        }
        public void SortByFitness()
        {
            var result = population
                .OrderByDescending(x => x.fitness)
                .ToList();

            population = new List<Chromosome>(result);
        }
        public void UpdateFitness(Func<List<double>, double> function)
        {
            foreach (var item in population)
            {
                item.UpdateIndividualFitness(function);
            }
        }
        public Population ArithmeticCrossover()
        {
            List<Chromosome> children = new List<Chromosome>();
            double k = 0.75;
            for (int i = 0; i < population.Count; i = i + 2)
            {
                if (population.Count - i >= 2)
                {
                    Chromosome child1 = population[i].Clone();
                    Chromosome child2 = population[i + 1].Clone();
                    for (int j = 0; j < child1.Genes.Count; j++)
                    {
                        child1.Genes[j] = population[i].Genes[j] + k * (population[i + 1].Genes[j] - population[i].Genes[j]);
                    }
                    for (int j = 0; j < child2.Genes.Count; j++)
                    {
                        child2.Genes[j] = population[i].Genes[j] + (population[i + 1].Genes[j] - child1.Genes[j]);
                    }
                    children.Add(child1);
                    children.Add(child2);
                }
                else
                {
                    Chromosome child = population[i].Clone();
                    for (int j = 0; j < child.Genes.Count; j++)
                    {
                        child.Genes[j] = population[i].Genes[j];
                    }
                    children.Add(child);
                }
            }
            return new Population(children);
        }
        public void MutatePopulation()
        {
            foreach (var item in population)
            {
                item.SingleMutation();
            }
        }
        public void ShowPopulation()
        {
            Console.WriteLine("///////////////////////////////////////////////");
            foreach (var item in population)
            {
                item.ShowIndividual();
            }
        }
        public void TakeTheBest()
        {
            var result = population
                .Select(x => x)
                .OrderByDescending(x => x.fitness)
                .First();
            Console.WriteLine("Najlepszy wynik: ");
            result.ShowIndividual();
        }
    }
}
