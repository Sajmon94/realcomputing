﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RealComputing
{
    class Program
    {
        static void Main(string[] args)
        {
            /*Przykład dla funkcji wielu zmiennych maximum = 22
            Population pop = new Population(20,2,-10,10);
            Func<List<double>, double> fitness = ((x) => -1*(2 * x[0]*x[0] + x[1])+12);
            pop.ShowPopulation();
            GeneticAlgorithm.RunSimulation(pop,fitness,1000);
            */
            //przykład dla funkcji jednej zmiennej(funkcja z zajęć)
            Population pop2 = new Population(10, 1, 0, 1024);
            Func<List<double>, double> fitness2 = (x => 2 * x[0] + 1);
            pop2.ShowPopulation();
            GeneticAlgorithm.RunSimulation(pop2, fitness2, 500);
        }
    }
}
