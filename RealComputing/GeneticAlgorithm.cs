﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RealComputing
{
    class GeneticAlgorithm
    {
        public static void RunSimulation(Population population,
                                         Func<List<double>, double> function,
                                         int iterationCount)
        {
            Population children;
            for (int i = 0; i < iterationCount; i++)
            {
                population.UpdateFitness(function);
                population.SortByFitness();
                children = population.ArithmeticCrossover();
                children.MutatePopulation();
                population.AddPopulations(children);
                population.SortByFitness();
                population.RemoveHalf();
                population.ShowPopulation();
            }
            population.TakeTheBest();
        }
    }
}
